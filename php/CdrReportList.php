<?php
namespace MyProject;

require 'VoipQ/VoipqApi.php';
use VoipQ\VoipqApi;

$config = require('config.php');

session_start();
$api = new VoipqApi($config);
$api->obtain_token();

$customerList = $api->customer_list();
if (!$customerList) {
    echo "No customers in this account!";
    return;
}

$year = 2019;
$month = 9;
$customer = $customerList[0];

$result = $api->cdr_report($year,$month, $customer->uuid);

echo "<h1>Report {$customer->name} {$year} / {$month}</h1>\r\n\r\n";
if ($result) {
    foreach ($result as $line) {
        echo $line->uuid . ";" .  $line->customerName . ";" .  $line->year . ";" .  $line->month . ";" .  $line->country . ";" . $line->destination
            . ";" . $line->count . ";" .  $line->duration . ";" .  $line->durationFormatted . ";" .  $line->amount . ";" .  $line->amountSale . "<br/>\r\n";
    }
}
