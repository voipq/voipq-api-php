<?php
namespace VoipQ;

require 'autoload.php';

use VoipQ\domain\CustomerInfo;
use VoipQ\domain\CallDetail;
use VoipQ\domain\ReportCdrDestinationLine;
use VoipQ\domain\PortingRequest;
use VoipQ\domain\DidNumber;
use VoipQ\domain\EchoMsg;
use VoipQ\common\Logger;

class VoipqApi
{
    var $config;

    var $base_url;
    var $token = '';
    var $last_error = '';

    /**
     * constructor.
     * @param $config array
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->base_url = $config['api.baseurl'];
        
        date_default_timezone_set('Europe/Amsterdam');

        $log = new Logger();
        $log->info('VoipqApi constructed. Base url: ' . $this->base_url);
    }
    
    public function use_token($token) 
    {
        $this->token = $token;
    }
    
    public function obtain_token() 
    {
        $client = new api\Oauth2Client(
            $this->config['api.username'],
            $this->config['api.password'],
            $this->config['api.clientid'],
            $this->config['api.secret']);

        $token = $client->get_cached_token();
        if ($token->access_token) {
            $this->token = $token->access_token;
        } else {
            $this->log_error('Error while obtaining token: ' . $token->error_description);
        }
    }
    
    /**
     * @param $message string The message you want to send
     * @return EchoMsg
     */
    public function test_echo($message) {
        $this->log_info('test_echo(' . $message . ')');
        
        $ch = curl_init();
        $this->curl_default_options($ch);
        curl_setopt($ch, 
            CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer ' . $this->token,
                    'Content-Type: application/json;charset=UTF-8'));
        curl_setopt($ch, 
            CURLOPT_URL, $this->base_url . 'test/echo/' . urlencode($message));

        $result = curl_exec($ch);
        $this->log_info(var_export(curl_getinfo($ch), true));
        curl_close($ch);

        return json_decode($result);
    }

    /**
     * 
     * @return PortingRequest[]
     */
    public function porting_request_list() 
    {
        $this->log_info('porting_request_list()');
        
        $ch = curl_init();
        $this->curl_default_options($ch);
        $headers = array(
            'Authorization: Bearer ' . $this->token,
            'Content-Type: application/json;charset=UTF-8',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->base_url . 'numberporting/');
        
        $result = curl_exec($ch);
        $this->check_error($ch);
        curl_close($ch);
        
        return json_decode($result);
    }

    /**
     *
     * @return CallDetail[]
     */
    public function cdr_search($year, $month, $day)
    {
        $this->log_info("cdr_search($year, $month)");

        $ch = curl_init();
        $this->curl_default_options($ch);
        $headers = array(
            'Authorization: Bearer ' . $this->token,
            'Content-Type: application/json;charset=UTF-8',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->base_url . 'cdr/search');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
            "year" => $year, "month" => $month, "day" => $day,
            "minUnitCount" => 1,
            "offset" => 0,
            "limit" => 1000)));
        
        $result = curl_exec($ch);
        $this->check_error($ch);
        curl_close($ch);

        return json_decode($result);
    }

    /**
     * @return ReportCdrDestinationLine[]
     */
    public function cdr_report($year, $month, $customerUuid) {
        $this->log_info("cdr_report($year, $month)");

        $ch = curl_init();
        $this->curl_default_options($ch);
        $headers = array(
            'Authorization: Bearer ' . $this->token,
            'Content-Type: application/json;charset=UTF-8',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->base_url . "report/cdr/bydestination/$year/$month/$customerUuid");

        $result = curl_exec($ch);
        $this->check_error($ch);
        curl_close($ch);

        return json_decode($result);
    }

    
    /**
     * 
     * @return CustomerInfo[]
     */
    public function customer_list() 
    {
        $this->log_info('customer_list()');
        
        $ch = curl_init();
        $this->curl_default_options($ch);
        $headers = array(
            'Authorization: Bearer ' . $this->token,
            'Content-Type: application/json;charset=UTF-8',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->base_url . 'customer/');
        
        $result = curl_exec($ch);
        $this->check_error($ch);
        curl_close($ch);
        
        return json_decode($result);
    }
    
    /**
     * 
     * @return DidNumber[]
     */
    public function didnumber_list() 
    {
        $this->log_info('didnumber_list()');
        
        $ch = curl_init();
        $this->curl_default_options($ch);
        $headers = array(
            'Authorization: Bearer ' . $this->token,
            'Content-Type: application/json;charset=UTF-8',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->base_url . 'didnumber/');
        
        $result = curl_exec($ch);
        $this->check_error($ch);
        curl_close($ch);
        
        return json_decode($result);
    }
    
    
    private function curl_default_options($ch) {
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    }

    private function log_info($msg) {
        $log = new Logger();
        $log->info($msg);
    }

    private function log_error($msg) {
        $log = new Logger();
        $log->error($msg);
    }
    
    private function check_error($ch) {
        $status = curl_getinfo($ch);
        if ($status['http_code'] != 200) {
            $this->log_error('Got status ' . $status['http_code']);
        }
    }
}
