<?php
namespace VoipQ\common;

class Logger
{
    var $file_name;

    public function __construct()
    {
        if (defined('LOG_DIR')) {
            $this->file_name = LOG_DIR . 'log_' . date('Y-m-d') . '.txt';
        }
    }

    public function info($msg) {
        if (empty($this->file_name)) {
            return;
        }
        
        $this->append(date('G:i:s') . ' INFO  ' . $msg . "\n");
    }

    public function error($msg) {
        if (empty($this->file_name)) {
            error_log(date('G:i:s') . ' ERROR ' . $msg);
            return;
        }
        
        $this->append(date('G:i:s') . ' ERROR ' . $msg . "\n");
    }


    private function append($line) {
        file_put_contents($this->file_name, $line, FILE_APPEND | LOCK_EX);
    }
}