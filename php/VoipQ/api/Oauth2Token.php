<?php
namespace VoipQ\api;

class Oauth2Token 
{
    var $token_type; // bearer

    var $access_token; // eyJhbGciOiJS....
    var $expires_in; // 300
    var $expires; // timestamp manually set

    var $refresh_token;
    var $refresh_expires_in; // 1800
    var $refresh_expires; // timestamp manually set

    var $id_token; // eyJhbGciOiJS...

    public function __construct() 
    {
        $this->token_type = 'bearer';
        $this->access_token = 'XXX';
        $this->expires_in = -1;
        $this->expires = 0;
        $this->refresh_token = 'XXX';
        $this->refresh_expires_in = -1;
        $this->refresh_expires = 0;
        $this->id_token = '';
    }
}