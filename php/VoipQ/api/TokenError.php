<?php
/**
 * An error when getting a token
 */

namespace VoipQ\api;

class TokenError
{
    var $error_description;
    var $error;
    var $refresh_expires;
    var $expires;
}