<?php
namespace VoipQ\api;

use VoipQ\common\Logger;

class Oauth2Client {
    var $authurl = 'https://voipq.nl/auth/realms/voipq/protocol/openid-connect/token';
    var $username;
    var $password;
    var $clientid;
    var $secret;

    /**
     * Oauth2Client constructor.
     * @param string $username
     * @param string $password
     * @param string $clientid
     * @param string $secret
     */
    public function __construct($username, $password, $clientid, $secret) {
        $this->username = $username;
        $this->password = $password;
        $this->clientid = $clientid;
        $this->secret = $secret;
    }

    /**
     * Reset the session oauth token
     */
    public function invalidate_token() {
        $this->log_info('invalidate_token');
        $token = new Oauth2Token();

        $this->to_session($token);
    }

    public function get_cached_token() {
        $this->log_info('get_cached_token');
        /** @var Oauth2Token | TokenError $token */
        $token = $this->from_session();

        if (!empty($token) && !isset($token->error)) {
            if (isset($token->expires) && time() < $token->expires) {
                $this->log_info('oauth2 token from session is still valid until ' . date('Y-m-d H:i:s', $token->expires));
                return $token;
            } else  if (isset($token->refresh_expires) && time() < $token->refresh_expires) {
                $this->log_info('oauth2 token needs to be refreshed, refresh still valid until '. date('Y-m-d H:i:s', $token->refresh_expires));
                $token = $this->refresh_token($token->refresh_token);
                if(isset($token->error)) {
                    $this->log_error("Error refreshing token: ". var_export($token, true));

                } else {
                    $this->to_session($token);
                }
                return $token;
            }
        }
        $this->log_info('getting new token');
        $token = $this->renew_token();
        if(isset($token->error)) {
            $this->log_error("Error renewing token: ". var_export($token, true));
        } else {
            $this->to_session($token);
        }
        return $token;
    }

    /**
     * @param $refresh_token string
     * @return Oauth2Token
     */
    private function refresh_token($refresh_token) {
        $this->log_info('refresh_token');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->authurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//          'Authorization: Basic ' . base64_encode($this->consumer_key . ':' . $this->consumer_secret),
            'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
        ));
        if (strlen($this->username) > 0) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=refresh_token&refresh_token=$refresh_token"
                . "&username={$this->username}&password={$this->password}&client_id={$this->clientid}");
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=refresh_token&refresh_token=$refresh_token"
                . "&client_id={$this->clientid}&client_secret={$this->secret}");
        }

        $result_data = curl_exec($ch);
        if ($result_data === FALSE) {
            $result_data = null;
        } else {
            $result_data = json_decode($result_data);
        }
        curl_close($ch);
        return $result_data;
    }

    /**
     * @return Oauth2Token | TokenError
     */
    private function renew_token() {
        $this->log_info('Renew token');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->authurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
        ));
        if (strlen($this->username) > 0) {
            $this->log_info("Password grant type");
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=password"
                . "&username={$this->username}&password={$this->password}&client_id={$this->clientid}");
        } else {
            $this->log_info("Client credentials grant type");
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials"
                . "&client_id={$this->clientid}&client_secret={$this->secret}");
        }

        $result_data = curl_exec($ch);
        if ($result_data === FALSE) {
            $result_data = null;
        } else {
            $result_data = json_decode($result_data);
        }
        curl_close($ch);
        return $result_data;
    }

    /**
     * @param $token Oauth2Token
     */
    private function to_session($token) {
        $token->refresh_expires = time() + $token->refresh_expires_in - 60;
        $token->expires = time() + $token->expires_in - 10;

        $_SESSION['apitoken'] = $token;
    }

    /**
     * @return Oauth2Token
     */
    private function from_session() {
        if (isset($_SESSION['apitoken'])) {
            return $_SESSION['apitoken'];
        }
        return new Oauth2Token();
    }

    /**
     * @param $message string
     */
    private function log_info($message) {
        $log = new Logger();
        $log->info($message);
    }

    /**
     * @param $message string
     */
    private function log_error($message) {
        $log = new Logger();
        $log->error($message);
    }

}
