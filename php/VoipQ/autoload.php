<?php
/**
 * A quick and dirty dependency list, migrate to composer...
 */
require 'common/Logger.php';

require 'domain/EchoMsg.php';
require 'domain/Contact.php';
require 'domain/CustomerInfo.php';
require 'domain/PortingRequest.php';
require 'domain/PortingRequestNumber.php';


require 'api/Oauth2Token.php';
require 'api/TokenError.php';
require 'api/Oauth2Client.php';

