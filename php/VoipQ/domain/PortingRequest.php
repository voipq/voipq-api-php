<?php
/**
 * Created by IntelliJ IDEA.
 * User: thiadmer
 * Date: 21/07/16
 * Time: 19:39
 */

namespace VoipQ\domain;


class PortingRequest
{
    var $requestDate;
    var $customer;
    var $portingID;

    var $donorServiceProvider;
    var $recipientServiceProvider;
    var $donorNetworkOperator;
    var $recipientNetworkOperator;

    /**
     * @var PortingRequestNumber[]
     */
    var $numbers;
    var $address;

    /**
     * @var Contact[]
     */
    var $contact;
    var $process;
    var $numberType;
    var $requestNote;
    var $internalNote;

    var $state;
    var $stateDate;
}