<?php
namespace VoipQ\domain;


class CallDetail
{
    var $id;
    // The name of the client who placed/received the call
    var $clientName;
    // A custom billing identifier used for sales invoicing
    var $billingCode;
    // The pbx extension identifier to which this call detail belongs
    var $extensionNumber;
    // The service of this call details record. Can be Voice/Data/SMS
    var $service;
    // Call flow, direction of the call: in/out
    var $flow;
    // The to number (B number) which was called in E.164 format
    var $calledNumber;
    // The from number (A number) of this call in E.164 format
    var $callingNumber;
    // The number digits extracted from the SIP-CallerID
    var $callerIdDigits;
    // The epoch second time stamp of the start of the call
    var $callStart;
    // The name of the destination group of which this prefix was part of
    var $destGroupName;
    // The destination country code in ISO3166 format
    var $destCountry;
    // The call disposition, like ANSWERED or FAILED
    var $disposition;
    // The number of units charged (like seconds in case of Voice calls)
    var $chargedUnitCount;
    // The unit which was used (sec)
    var $chargedUnitType;
    // The total charge for this call (4 decimals)
    var $chargedTotal;
    // Indication of costs which can be charged to end customers (4 decimals)
    var $chargedTotalSale;
    // The Invoice year of this record determined by UTC call start
    var $year;
    // The Invoice month of this record determined by UTC call start
    var $month;
    // The Invoice day of momth of this record determined by UTC call start
    var $day;
}
