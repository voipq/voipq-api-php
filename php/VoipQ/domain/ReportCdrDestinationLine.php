<?php
/**
 * Created by IntelliJ IDEA.
 * User: thiadmer
 * Date: 21/07/16
 * Time: 19:39
 */

namespace VoipQ\domain;


class ReportCdrDestinationLine
{
    var $uuid;
    var $customerName;
    var $year;
    var $month;
    var $country;
    var $destination;
    var $count;
    var $duration;
    var $durationFormatted;
    var $amount;
    var $amountSale;
}
