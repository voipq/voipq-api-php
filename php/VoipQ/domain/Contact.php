<?php
/**
 * Created by IntelliJ IDEA.
 * User: thiadmer
 * Date: 21/07/16
 * Time: 19:39
 */

namespace VoipQ\domain;


class Contact
{
    var $initials;
    var $lastName;
    var $email;
    var $phone;
}