<?php
/**
 * Created by IntelliJ IDEA.
 * User: thiadmer
 * Date: 21/07/16
 * Time: 19:39
 */

namespace VoipQ\domain;


class PortingRequestNumber
{
    var $item;
    var $numberStart;
    var $numberEnd;
    var $blockingCode;
    var $blockingNote;
}