<?php
namespace VoipQ\domain;

class CustomerInfo
{
    /**
     * @var string
     */
    var $uuid;

    /**
     * @var string
     */
    var $name;

    /**
     * @var Contact
     */
    var $contact;
}