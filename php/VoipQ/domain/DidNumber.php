<?php
/**
 * Created by IntelliJ IDEA.
 * User: thiadmer
 * Date: 24/07/16
 * Time: 13:21
 */

namespace VoipQ\domain;


class DidNumber
{
    var $phoneNumber;
    var $blockSize;
    var $stateDate;
    var $state;

    /**
     * @var CustomerInfo
     */
    var $customer;
}