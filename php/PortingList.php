<?php
namespace MyProject;

require 'VoipQ/VoipqApi.php';
use VoipQ\VoipqApi;

$config = require('config.php');

session_start();
$api = new VoipqApi($config);
$api->obtain_token();

$portingList = $api->porting_request_list();

echo "<h1>Portings</h1>";
if ($portingList) {
    foreach ($portingList as $porting) {
//        var_dump($porting);
        echo "<li>" . $porting->portingID . "<br/>";
    }
}