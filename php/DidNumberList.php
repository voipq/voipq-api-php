<?php
namespace MyProject;

require 'VoipQ/VoipqApi.php';
use VoipQ\VoipqApi;

$config = require('config.php');

session_start();
$api = new VoipqApi($config);
$api->obtain_token();

$didNumberList = $api->didnumber_list();

echo "<h1>Numbers</h1>";
echo "<table>";
if ($didNumberList) {
    foreach ($didNumberList as $didNumber) {
        echo "<tr><td>{$didNumber->phoneNumber} </td><td>{$didNumber->customer->name} </td></tr>";
    }
}
echo "</table>";