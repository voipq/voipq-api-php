<?php
namespace MyProject;

require 'VoipQ/VoipqApi.php';
use VoipQ\VoipqApi;

$config = require('config.php');

session_start();
$api = new VoipqApi($config);
$api->obtain_token();

$response = $api->test_echo('Hello world');
echo $response->message;