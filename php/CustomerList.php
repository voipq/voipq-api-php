<?php
namespace MyProject;

require 'VoipQ/VoipqApi.php';
use VoipQ\VoipqApi;

$config = require('config.php');

session_start();
$api = new VoipqApi($config);
$api->obtain_token();

$customerList = $api->customer_list();

echo "<h1>Customers</h1>";
if ($customerList) {
    foreach ($customerList as $customer) {
        echo $customer->name . "<br/>";
    }
}