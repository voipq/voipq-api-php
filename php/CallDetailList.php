<?php
namespace MyProject;

require 'VoipQ/VoipqApi.php';
use VoipQ\VoipqApi;

$config = require('config.php');

session_start();
$api = new VoipqApi($config);
$api->obtain_token();

$cdrList = $api->cdr_search(2019, 9, 1);

echo "<h1>cdr list</h1>";
if ($cdrList) {
    foreach ($cdrList as $cdr) {
        echo $cdr->day . "  " .  $cdr->id . " " . $cdr->chargedUnitCount . "<br/>";
    }
}
