<?php
/**
 * Rename this file to config.php and customize
 */


/**
 * Demo configuration for password auth flow
 */
return array(
    'api.baseurl'   => 'https://voipq.nl/api/v4.0/',
    'api.username'  => 'user@company.nl', // username or email here
    'api.password'  => 'sfkskfjhslkjfhw', // your (portal) password here
    'api.clientid'  => 'voipq-api', // the public client id to access the api
    'api.secret'  => '', // empty
);


/**
 * Demo configuration for client_credentials flow
*/
// return array(
//     'api.baseurl'   => 'https://voipq.nl/api/v4.0/',
//     'api.username'  => '', // keep empty
//     'api.password'  => '', // keep empty
//     'api.clientid'  => 'client-company', // or your own clientid
//     'api.secret'    => '32f4f9av-4ce9-12a1-565a-31516e8d3364', // your client secret
// );

