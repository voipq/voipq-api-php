# VoipQ API v4.0 PHP code #

### Requirements ###
1. PHP 5.5 or newer
1. Your [VoipQ](https://voipq.nl) API credentials

### Quick start (mac/linux) ###

```
cd your/project/folder
git clone https://bitbucket.org/voipq/voipq-api-php.git
cd voipq-api-php
mv php/config-sample.php php/config.php
vi php/config.php
php -S localhost:8000 -t php/
```
and navigate to http://localhost:8000/TestEcho.php


### Further steps ###

1. [Learn](https://www.atlassian.com/git/tutorials/) git
1. [Fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) this project
1. [Build](http://www.phptherightway.com/) your own unique customer solution in PHP
1. [Keep](https://www.atlassian.com/git/articles/git-forks-and-upstreams/) in sync with this upstream repo


# The demo code #

### php/EchoMsg.php ###
Echo a message.

### php/CustomerList.php ###
List all customers names.

### php/DidNumberList.php ###
List all did numbers in your account.